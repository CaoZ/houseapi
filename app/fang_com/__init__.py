from util.orm import Base, engine

from .record import District, PriceRecord

Base.metadata.create_all(engine)
