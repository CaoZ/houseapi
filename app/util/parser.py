from jsonschema import Draft4Validator, FormatChecker, ValidationError
from tornado import escape


def parse_json_request(request_body, schema, check_format=False):
    """
    parse json request, raise ValueError if error occurred.

    :param request_body: json 字符串
    :param schema: json schema
    :param check_format: 是否同时检查格式, 如果 schema 有 format 要求, 则检查, 否则不检查以提高速度.
    :return: parsed data
    """
    try:
        args = escape.json_decode(request_body)
    except Exception:
        raise ValueError('Bad json data.')

    if check_format:
        the_validator = Draft4Validator(schema, format_checker=FormatChecker())
    else:
        the_validator = Draft4Validator(schema)

    try:
        the_validator.validate(args)

    except ValidationError as e:
        if e.absolute_path:
            # 某一字段有误
            wrong_field = e.absolute_path.pop()

            if e.schema.get('type') == 'string' and e.validator in ('pattern', 'minLength', 'maxLength'):
                if e.validator == 'pattern':
                    error_msg = '字段 "{}" 格式有误.'.format(wrong_field)
                else:
                    error_msg = '字段 "{}" 长度有误.'.format(wrong_field)

            else:
                error_msg = 'Wrong field "{}": {}'.format(wrong_field, e.message)

        else:
            # "The deque can be empty if the error happened at the root of the instance", 可能 instance 本身结构有误
            error_msg = 'Bad data: {}'.format(e.message)

        raise ValueError(error_msg)

    if isinstance(args, dict) and not args:
        # 不允许空 json
        raise ValidationError('Bad data: no arguments found.')

    return args


def parse_version(version_string):
    """
    parse version string like '2.27.1', at most three part (major, minor, build)
    @return: a tuple
    """
    version = []
    split = version_string.split('.', 3)[:3]

    for part in split:
        try:
            version.append(int(part))
        except ValueError:
            break

    if version[-1] == 0:
        # 若最后一项是0, 没什么意义, 删除之
        del version[-1]

    if not version:
        # 若没有得到版本号, 认为是1.0
        version.append(1)

    return tuple(version)
