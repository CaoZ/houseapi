import json

from sqlalchemy import types
from sqlalchemy.ext.compiler import compiles
from tornado import escape


@compiles(types.DateTime, 'postgresql')
def pg_datetime(element, compiler, **kw):
    """
    去掉 PostgreSQL 中 TIMESTAMP 秒的小数部分
    """
    return 'TIMESTAMP(0)'


def json_encode(value):
    """
    重写 tornado 中的 json_encode, 不 ensure_ascii, 便于人类阅读...
    """
    return json.dumps(value, ensure_ascii=False).replace("</", "<\\/")


def do_patch():
    escape.json_encode = json_encode
