from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop
from tornado.options import define, options
from tornado.web import Application

import monkey
from config import config
from server import TrendHandler


def create_app(port):
    settings = {
        'debug': config.debug
    }

    server_settings = {
        'xheaders': True
    }

    handlers = [
        (r'/1.0/trend', TrendHandler)
    ]

    application = Application(handlers, **settings)
    server = HTTPServer(application, **server_settings)
    server.listen(port)


def start():
    # 启动
    define('port', default=8000)
    define('main_process', default=True)

    options.parse_command_line()

    print('Starting... [port={}]'.format(options.port))
    create_app(options.port)

    # 启动成功
    print('App starred!')
    IOLoop.current().start()


def main():
    monkey.do_patch()
    start()


if __name__ == '__main__':
    main()
