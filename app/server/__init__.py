from bjjs_gov import TransactionRecord
from fang_com import PriceRecord
from util.web import BaseRequestHandler


class TrendHandler(BaseRequestHandler):
    def get(self):
        transaction_data = TransactionRecord.get_data(self.db_session)
        price_trend = PriceRecord.get_price_trend(self.db_session)
        latest_prices = PriceRecord.get_latest_prices(self.db_session)

        self.finish({
            'transaction_data': transaction_data,
            'price_trend': price_trend,
            'latest_prices': latest_prices
        })
