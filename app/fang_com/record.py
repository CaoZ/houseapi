from datetime import datetime

from sqlalchemy import Column, types
from sqlalchemy.dialects.postgresql import JSONB

from util.orm import AlchemyMixin, Base

# http://www.tcmap.com.cn/beijing/

DISTRICT_MAP = {
    '东城': 110101,
    '西城': 110102,
    '朝阳': 110105,
    '丰台': 110106,
    '石景山': 110107,
    '海淀': 110108,
    '门头沟': 110109,
    '房山': 110111,
    '通州': 110112,
    '顺义': 110113,
    '昌平': 110114,
    '大兴': 110115,
    '怀柔': 110116,
    '平谷': 110117,
    '密云': 110228,
    '延庆': 110229,
    '全市': 999999
}


class District(AlchemyMixin, Base):
    __tablename__ = 'districts'

    id = Column(types.Integer, primary_key=True)
    name = Column(types.String, nullable=False)


class PriceRecord(AlchemyMixin, Base):
    __tablename__ = 'price_records_fang_com'

    id = Column(types.Integer, primary_key=True)
    # iso 格式时间, 如 2017-01
    time = Column(types.CHAR(7), nullable=False, unique=True, index=True)
    data = Column(JSONB, nullable=False)
    updated_at = Column(types.DateTime, nullable=False, default=datetime.now)

    def __init__(self, year, month, data):
        self.time = '{}-{:02d}'.format(year, month)
        self.data = data

    @classmethod
    def get_price_trend(cls, session, months=12):
        the_records = session.query(
            cls.time, cls.updated_at, cls.data[('全市', 'unit_price')].label('unit_price')
        ).order_by(cls.time.desc())[:months]

        data = []

        for i in range(len(the_records)):
            record = the_records[i]

            data.append({
                'label': '{}月'.format(int(record.time[-2:])),
                'value': record.unit_price
            })

        return {
            'updated_at': the_records[0].updated_at.date().isoformat(),
            'title': '成交均价走势',
            'unit': '元/平方',
            'source': '网络',
            'type': 'line',
            'data': data
        }

    @classmethod
    def get_latest_prices(cls, session):
        the_record = session.query(cls).order_by(cls.time.desc()).first()
        data = []

        for k, v in the_record.data.items():
            data.append({
                'label': k,
                'value': v['unit_price']
            })

        data.sort(key=lambda x: DISTRICT_MAP[x['label']])

        return {
            'updated_at': the_record.updated_at.date().isoformat(),
            'title': '各区参考价',
            'unit': '元/平方',
            'source': '网络',
            'type': 'list',
            'data': data
        }
