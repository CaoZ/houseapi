from datetime import datetime

from sqlalchemy import Column, types

from util.orm import AlchemyMixin, Base, engine


class TransactionRecord(AlchemyMixin, Base):
    __tablename__ = 'transaction_records'

    id = Column(types.Integer, primary_key=True)
    date = Column(types.Date, nullable=False, unique=True, index=True)
    online_transaction_count = Column(types.Integer, nullable=False)
    online_transaction_area = Column(types.Float, nullable=False)
    house_transaction_count = Column(types.Integer, nullable=False)
    house_transaction_area = Column(types.Float, nullable=False)
    updated_at = Column(types.DateTime, nullable=False, default=datetime.now)

    @classmethod
    def get_data(cls, session, days=30):
        the_records = session.query(cls).order_by(cls.date.desc())[:days]
        data = []

        for i in range(len(the_records)):
            record = the_records[i]

            data.append({
                'label': '{}日'.format(record.date.day),
                'value': record.house_transaction_count
            })

        return {
            'updated_at': the_records[0].date.isoformat(),
            'title': '网签数量',
            'unit': '网签量/套',
            'source': '建委官网',
            'type': 'column',
            'data': data
        }


Base.metadata.create_all(engine)
