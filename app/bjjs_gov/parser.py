import logging
from datetime import datetime

import os
import re
import requests
from pyquery import PyQuery

from bjjs_gov import TransactionRecord
from config import config
from util.orm import Session


def get_data():
    url = 'http://www.bjjs.gov.cn/bjjs/fwgl/fdcjy/fwjy/index.shtml'
    r = requests.get(url)
    document = PyQuery(r.text)
    trs = document(
        '#daf461d430a34e6e877f5246ae33e36f > div:nth-of-type(2) > table:nth-of-type(2) > tr > td:nth-of-type(4) > table > tr'
    )

    try:
        date_pattern = r'[\d/]+'
        date_string = re.search(date_pattern, trs.eq(0)('td').text()).group()
        date = datetime.strptime(date_string, '%Y/%m/%d').date()
        online_transaction_count = int(trs.eq(1)('td:nth-of-type(2)').text())
        online_transaction_area = float(trs.eq(2)('td:nth-of-type(2)').text())
        house_transaction_count = int(trs.eq(3)('td:nth-of-type(2)').text())
        house_transaction_area = float(trs.eq(4)('td:nth-of-type(2)').text())

        data = TransactionRecord(
            date=date,
            online_transaction_count=online_transaction_count,
            online_transaction_area=online_transaction_area,
            house_transaction_count=house_transaction_count,
            house_transaction_area=house_transaction_area
        )

        logging.info('成功获得数据: {}'.format(data.date))
        return data

    except Exception as e:
        logging.error('页面结构可能有变化, 获取签约数据失败: {}'.format(repr(e)))
        return None


def fill_data():
    session = Session()

    have = session.query(TransactionRecord.date).order_by(TransactionRecord.date.desc()).first()

    if have:
        have = have[0]

    data = get_data()

    if data:
        if have == data.date:
            logging.info('数据已是最新, 无需更新.')
        else:
            session.add(data)
            session.commit()
            logging.info('数据更新完毕.')

    else:
        logging.info('获取数据失败.')

    session.close()


def proxy_patch():
    """
    Requests 似乎不能使用系统的证书系统, 方便起见, 不验证 HTTPS 证书, 便于使用代理工具进行网络调试...
    http://docs.python-requests.org/en/master/user/advanced/#ca-certificates
    """
    import warnings
    from requests.packages.urllib3.exceptions import InsecureRequestWarning

    class XSession(requests.Session):
        def __init__(self):
            super().__init__()
            self.verify = False

    requests.Session = XSession
    warnings.simplefilter('ignore', InsecureRequestWarning)


if __name__ == '__main__':
    if config.debug and os.getenv('HTTPS_PROXY'):
        proxy_patch()

    fill_data()
