import logging
import math
from datetime import date

import os
import re
import requests
from pyquery import PyQuery

from config import config
from fang_com.record import DISTRICT_MAP, PriceRecord
from util.orm import Session

# e.g. http://esf.fang.com/top/DisCjList-mo201703/
URL_PATTERN = 'http://esf.fang.com/top/DisCjList-mo{YYYY}{MM:02d}/'


def get_data(year, month):
    logging.info('获取 {}-{:02d} 的数据...'.format(year, month))

    url = URL_PATTERN.format(YYYY=year, MM=month)
    r = requests.get(url)
    document = PyQuery(r.text)
    records = document('.bdlistdiv li:gt(0)')

    if not records.length:
        return None

    data = {}
    price_total = 0
    transaction_total = 0

    for record in records.items():
        spans = record('span:gt(0)')
        district = spans.eq(0).text()
        assert district in DISTRICT_MAP, '错误的行政区: {}'.format(district)
        unit_price = int(re.search(r'\d+', spans.eq(1).text()).group(0))
        transaction_volume = int(re.search(r'\d+', spans.eq(2).text()).group(0))

        data[district] = {
            'unit_price': unit_price,
            'transaction_volume': transaction_volume
        }

        price_total += unit_price * transaction_volume
        transaction_total += transaction_volume

    # 添加汇总数据
    data['全市'] = {
        'unit_price': math.ceil(price_total / transaction_total),
        'transaction_volume': transaction_total
    }

    return data


def get_all_data():
    session = Session()

    today = date.today()
    have = session.query(PriceRecord.time).order_by(PriceRecord.time.desc()).first()

    if have:
        have = have[0]
        year_begin = int(have[:4])
        month_begin = int(have[-2:]) + 1
    else:
        year_begin = 2015
        month_begin = 1

    year_end = today.year

    for year in range(year_begin, year_end + 1):
        # 数据开始于 2015年1月, 截至到上月
        month_end = today.month - 1 if year == today.year else 12

        for month in range(month_begin, month_end + 1):
            data = get_data(year, month)

            if data:
                record = PriceRecord(year, month, data)
                session.add(record)

        # month_begin 只对首年有效
        month_begin = 1

    session.commit()

    logging.info('数据填充完毕.')


def proxy_patch():
    """
    Requests 似乎不能使用系统的证书系统, 方便起见, 不验证 HTTPS 证书, 便于使用代理工具进行网络调试...
    http://docs.python-requests.org/en/master/user/advanced/#ca-certificates
    """
    import warnings
    from requests.packages.urllib3.exceptions import InsecureRequestWarning

    class XSession(requests.Session):
        def __init__(self):
            super().__init__()
            self.verify = False

    requests.Session = XSession
    warnings.simplefilter('ignore', InsecureRequestWarning)


if __name__ == '__main__':
    if config.debug and os.getenv('HTTPS_PROXY'):
        proxy_patch()

    get_all_data()
