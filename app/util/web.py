from sqlalchemy import event
from tornado.web import RequestHandler

from util import parser
from util.orm import Session


class BaseRequestHandler(RequestHandler):
    """
    扩展了的 RequestHandler, 包含了一些此项目中常用到的方法.
    """
    TOKEN_NAME = 'auth-token'
    AUTH_MAX_AGE = 365

    def parse_json_request_or_quit(self, schema, check_format=False):
        """
        分析 json 请求, 如果请求数据格式有误, 向客户端发送错误信息并结束请求.
        :return: parsed request data or None if error occurred
        """
        try:
            args = parser.parse_json_request(self.request.body, schema, check_format=check_format)
            return args
        except ValueError as e:
            self.send_error_message(400, str(e))

    @property
    def host_path(self):
        """
        运行时的网址 host 部分, 如: http://api.anyview.net
        """
        if not hasattr(self, '_host_path'):
            self._host_path = self.request.protocol + '://' + self.request.host
        return self._host_path

    def set_content_type_json(self):
        """
        设置返回的 Content-Type 为 json.
        """
        self.set_header('Content-Type', 'application/json; charset=utf-8')

    @property
    def db_session(self):
        if not hasattr(self, '_db_session'):
            self._db_session = Session()
            self._need_close_session = True

            @event.listens_for(self._db_session, 'after_commit')
            def receive_after_commit(session):
                # commit 后就没必要再 close session 了. http://docs.sqlalchemy.org/en/latest/orm/session.html
                self._need_close_session = False

            @event.listens_for(self._db_session, 'after_begin')
            def receive_after_begin(session, transaction, connection):
                self._need_close_session = True

        return self._db_session

    def options(self, *args, **kwargs):
        # 解决请求跨域问题
        # https://developer.mozilla.org/zh-CN/docs/Web/HTTP/Access_control_CORS

        is_preflight_request = ('Access-Control-Request-Method' in self.request.headers or
                                'Access-Control-Allow-Headers' in self.request.headers)

        if is_preflight_request:
            self.add_header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE')

            # 设置缓存时间, 但这个时间可能小于浏览器允许的最大时间
            # http://stackoverflow.com/questions/23543719/cors-access-control-max-age-is-ignored
            self.add_header('Access-Control-Max-Age', 86400 * 10)  # 10天

        if 'Access-Control-Request-Headers' in self.request.headers:
            self.add_header('Access-Control-Allow-Headers', self.request.headers['Access-Control-Request-Headers'])

        self.finish()

    def finish(self, chunk=None):
        # 解决请求跨域问题

        if 'Origin' in self.request.headers:
            self.add_header('Access-Control-Allow-Origin', '*')

        super(BaseRequestHandler, self).finish(chunk)

    def on_finish(self):
        if hasattr(self, '_db_session') and self._need_close_session:
            self._db_session.close()
            pass

    def send_error_message(self, status_code, message):
        """
        返回 json 格式的错误信息
        """
        self.set_status(status_code)
        self.finish(dict(message=message))

    def get_argument_int(self, argument_name, default=None):
        """
        获取参数, 返回 int or default
        :return: int or default
        """
        the_value = self.get_argument(argument_name, default)

        try:
            the_value = int(the_value)
        except Exception:
            the_value = default

        return the_value
